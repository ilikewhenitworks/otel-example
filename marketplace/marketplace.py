import os

from flask import Flask, render_template
import grpc

from opentelemetry import trace
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.sdk.resources import Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor

from api_pb2 import BookCategory, RecommendationRequest
from api_pb2_grpc import RecommendationsStub


resource = Resource(attributes={
    "service.name": "Marketplace",
    "tracing.persist": True
})
trace.set_tracer_provider(TracerProvider(resource=resource))
tracer = trace.get_tracer(__name__)
otlp_exporter = OTLPSpanExporter(endpoint=os.getenv("OTEL_EXPORTER_OTLP_ENDPOINT", "localhost"), insecure=True)
span_processor = BatchSpanProcessor(otlp_exporter)
trace.get_tracer_provider().add_span_processor(span_processor)


app = Flask(__name__)

recommendations_host = os.getenv("RECOMMENDATIONS_HOST", "localhost")
recommendations_channel = grpc.insecure_channel(
    f"{recommendations_host}:50051"
)
recommendations_client = RecommendationsStub(recommendations_channel)


@app.route("/")
def render_homepage():
    with tracer.start_as_current_span("Request") as rec:
        recommendations_request = RecommendationRequest(
            user_id=1, category=BookCategory.MYSTERY, max_results=3
        )
        with tracer.start_as_current_span("Request child") as rec:
            recommendations_response = recommendations_client.recommend(
                recommendations_request,
            )
        return render_template(
            "homepage.html",
            recommendations=recommendations_response.recommendations,
        )


if __name__ == "__main__":
    app.run()
