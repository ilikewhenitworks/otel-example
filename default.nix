{ }:

let
  pkgs = import <nixpkgs> { };
in
  pkgs.stdenv.mkDerivation {
    name = "otel-collector-custom";
    src = ./.;
    buildInputs = [
      pkgs.docker-compose
      pkgs.go
      pkgs.python310Full
      pkgs.python310Packages.flask
      pkgs.python310Packages.grpcio-tools
      pkgs.python310Packages.jinja2
      pkgs.python310Packages.jupyter
    ];
  }
